﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Word
{
    public string Name;
    public string Category;

    private string _spriteName;

    public Sprite Sprite
    {
        get
        {
            return Resources.Load<Sprite>(_spriteName);
        }
    }

    public Word(string name, string spriteName)
    {
        Name = name;
        _spriteName = spriteName;
    }

    public Word(string category, string name, string spriteName)
    {
        Name = name;
        Category = category;
        _spriteName = spriteName;
    }
}

public class TSVReader : MonoBehaviour {

    public TextAsset TSV;
    public string ImagesPath = "";

    private List<List<Word>> _words = new List<List<Word>>();

    public List<Word> Words
    {
        get
        {
            if (_words.Count <= 0)
                ReadTSV();

            return _words[0];

            switch (WordGameController.Instance.LettersToComplete)
            {
                default:
                case LettersToComplete.None:
                    return null;
                case LettersToComplete.Vowels:
                    return _words[0];
                case LettersToComplete.Consonants:
                    return _words[1];
                case LettersToComplete.Encounters:
                    return _words[2];
                case LettersToComplete.All:
                    return _words[3];
            }
        }
    }

    private void Awake()
    {
        ReadTSV();
    }

    private void ReadTSV()
    {
        _words = new List<List<Word>>() { new List<Word>(), new List<Word>(), new List<Word>(), new List<Word>()};

        if (TSV == null)
        {
            Debug.LogWarning("NULL TSV");

            print(Resources.Load<Sprite>($"{ImagesPath}/dog_colored").name);

            _words[0].Add(new Word("Cachorro", $"{ImagesPath}/dog_colored"));
            _words[0].Add(new Word("Ovelha", $"{ImagesPath}/sheep_colored"));
            _words[0].Add(new Word("Vaca", $"{ImagesPath}/cow_colored"));
            _words[0].Add(new Word("Galinha", $"{ImagesPath}/chicken_colored"));

            _words[1] = new List<Word>(_words[0]);
            _words[2] = new List<Word>(_words[0]);
            _words[3] = new List<Word>(_words[0]);
            return;
        }

        string tsv = TSV.text;

        string[] lines = tsv.Split('\n');

        for (int i = 1; i < lines.Length; i++)
        {
            string[] columns = lines[i].Split('\t');

            if (string.IsNullOrEmpty(columns[0]))
                continue;

            //print($"{columns[1]} - {columns[3]}");

            Word word = new Word(columns[0], columns[1], $"{ImagesPath}/{columns[3].Trim()}");
            _words[0].Add(word);
        }

        _words[1] = new List<Word>(_words[0]);
        _words[2] = new List<Word>(_words[0]);
        _words[3] = new List<Word>(_words[0]);
    }
}
