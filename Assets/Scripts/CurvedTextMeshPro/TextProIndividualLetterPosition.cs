﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ntw.CurvedTextMeshPro;

[ExecuteInEditMode]
public class TextProIndividualLetterPosition : TextProOnACurve
{
    [System.Serializable]
    public class CharData
    {
        public Vector3 Translation;
    }

    public List<CharData> Data = new List<CharData>();
    
    private List<CharData> _oldData;
    private string _oldText;

    private TextMeshProUGUI _tmPro;
    public TextMeshProUGUI TMPro
    {
        get
        {
            if (_tmPro == null)
                _tmPro = GetComponent<TextMeshProUGUI>();

            return _tmPro;
        }
    }

    protected override bool ParametersHaveChanged()
    {
        if (_oldText != TMPro.text)
        {
            for (int i = 0; i < TMPro.text.Length; i++)
            {
                if (i >= Data.Count)
                {
                    Data.Add(new CharData());
                }
            }

            if (TMPro.text.Length < Data.Count)
            {
                Data.RemoveRange(TMPro.text.Length, Data.Count - TMPro.text.Length);
            }

            _oldData = new List<CharData>();
            foreach (var d in Data)
            {
                CharData data = new CharData();
                data.Translation = d.Translation;
                _oldData.Add(data);
            }
            return true;
        }

        if (_oldData == null || _oldData.Count != Data.Count)
        {
            _oldData = new List<CharData>();
            foreach (var d in Data)
            {
                CharData data = new CharData();
                data.Translation = d.Translation;
                _oldData.Add(data);
            }
            return true;
        }

        for (int i = 0; i < Data.Count; i++)
        {
            if (Data[i].Translation != _oldData[i].Translation)
            {
                _oldData = new List<CharData>();
                foreach (var d in Data)
                {
                    CharData data = new CharData();
                    data.Translation = d.Translation;
                    _oldData.Add(data);
                }
                return true;
            }
        }
        
        return false;
    }

    protected override Matrix4x4 ComputeTransformationMatrix(Vector3 charMidBaselinePos, float zeroToOnePos, TMP_TextInfo textInfo, int charIdx)
    {
        if (Data.Count > charIdx)
            return Matrix4x4.Translate(charMidBaselinePos + Data[charIdx].Translation);

        return Matrix4x4.Translate(charMidBaselinePos);
    }
}
