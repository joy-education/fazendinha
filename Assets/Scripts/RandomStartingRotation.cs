﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomStartingRotation : MonoBehaviour
{
    public Vector3 MinRotation = new Vector3(0, 0, -10);
    public Vector3 MaxRotation = new Vector3(0, 0, 10);

    private void OnEnable()
    {
        float x = Random.Range(MinRotation.x, MaxRotation.x);
        float y = Random.Range(MinRotation.y, MaxRotation.y);
        float z = Random.Range(MinRotation.z, MaxRotation.z);
        Vector3 rot = new Vector3(x, y, z);

        transform.eulerAngles = rot;
    }
}
