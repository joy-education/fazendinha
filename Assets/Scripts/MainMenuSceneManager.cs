﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceneManager : MonoBehaviour
{
    public Openable Fader;
    public LogoAnimation LogoAnimation;
    public Openable GameModeSelection;

    private void Start()
    {
        Fader.SetActive(false, () => 
        {
            if (!LogoAnimation.HasPlayed)
            {
                LogoAnimation.Play(() => 
                {
                    AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.LetsPlay, 2f);
                });

                AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Title, 2f);
            }
            else
                GameModeSelection.SetActive(true);
        });

        AudioController.PlayMusic(AudioController.MusicList.MainMenu);
    }

    public void MenuWordsButtonClicked()
    {
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Words);
    }

    public void MenuNumbersButtonClicked()
    {
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Numbers);
    }

    public void BackButtonClicked()
    {
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Back);
    }

    public void QuitButtonClicked()
    {
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.CloseApp;
        AudioController.PlayVoice(voice);
        Fader.SetActive(true);
        CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () => 
        {
            Application.Quit();
        });
    }

    public void LettersButtonClicked()
    {
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.AlphabetLetters;
        ChangeScene("LettersPreview", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void VowelsButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.Vowels;
        GameConfigs.MathOperation = MathOperation.None;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Vowels;
        ChangeScene("WordsGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void ConsonantsButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.Consonants;
        GameConfigs.MathOperation = MathOperation.None;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Consonants;
        ChangeScene("WordsGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void FullWordsButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.All;
        GameConfigs.MathOperation = MathOperation.None;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Dictation;
        ChangeScene("WordsGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void NumbersButtonClicked()
    {
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Numbers;
        ChangeScene("NumbersPreview", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void SumButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.None;
        GameConfigs.MathOperation = MathOperation.Addition;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Addition;
        ChangeScene("NumbersGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void SubtractionButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.None;
        GameConfigs.MathOperation = MathOperation.Subtraction;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Subtraction;
        ChangeScene("NumbersGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void MultiplicationButtonClicked()
    {
        GameConfigs.LettersToComplete = LettersToComplete.None;
        GameConfigs.MathOperation = MathOperation.Multiplication;

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Multiplication;
        ChangeScene("NumbersGame", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void PaintButtonClicked()
    {
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Painting;
        ChangeScene("PaintMode", voice.length);
        AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);
        AudioController.PlayVoice(voice);
    }

    public void CreditsButtonClicked()
    {
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Credits);
    }

    public void SoundButtonClicked()
    {
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.ConfigSound);
    }

    public void ChangeScene(string sceneName)
    {
        AudioController.StopMusic();
        Fader.SetActive(true, () =>
        {
            SceneManager.LoadScene(sceneName);
        });
    }

    public void ChangeScene(string sceneName, float audioLenght)
    {
        AudioController.StopMusic();
        Fader.SetActive(true);
        CoroutineManager.WaitForSeconds(Mathf.Max(audioLenght, Fader.Tween.Duration), () =>
        {
            SceneManager.LoadScene(sceneName);
        });
    }
    
}
