﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    public class HoldButon : Button
    {
        [SerializeField]
        public ButtonClickedEvent onPressed;

        private bool _pressed = false;

        private void Update()
        {
            if (_pressed)
                onPressed.Invoke();
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            _pressed = true;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            _pressed = false;
        }
    }
}
