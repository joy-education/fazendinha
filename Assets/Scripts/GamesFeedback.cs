﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GamesFeedback : Openable
{

    private TextMeshProUGUI _text;
    public TextMeshProUGUI Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<TextMeshProUGUI>();

            return _text;
        }
    }

    public void SetActive(bool active, string text, System.Action callback = null)
    {
        Text.text = text;
        SetActive(active, callback);
    }

}
