﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level1AnswerSlot : MonoBehaviour {

    public Sprite DotSprite;
    public RectTransform DotsArea;
    public GameObject ObjectPrefab;

    private List<GameObject> _dots = new List<GameObject>();

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
            {
                _tween = PotaTween.Create(gameObject);
                _tween.SetAlpha(0f, 1f).SetDuration(0.2f);
            }
            return _tween;
        }
    }

    public void SetAnswerImage(int answer, System.Action callback = null)
    {
        /*RevImageTween.Reverse(() => 
        {
            AnswerImage.sprite = AnswerSprites[answer];
            AnswerImage.SetNativeSize();

            ImageTween.Stop();
            ImageTween.Play(callback);

        });*/

        CreateDots(answer, callback);
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        Tween.Stop();

        if (active)
        {
            gameObject.SetActive(true);
            Tween.Play(callback);
        }
        else
        {
            DestroyDots(() => 
            {
                Tween.Reverse(() =>
                {
                    gameObject.SetActive(false);

                    if (callback != null)
                        callback();
                });
            });
            /*SetAnswerImage(0, () => 
            {
                Tween.Reverse(() => 
                {
                    gameObject.SetActive(false);

                    if (callback != null)
                        callback();
                });
            });*/
        }
    }

    private void CreateDots(int number, System.Action callback)
    {
        _dots = new List<GameObject>();

        for (int i = 0; i < number; i++)
        {
            /*GameObject dot = new GameObject("Dot" + i);
            dot.transform.SetParent(transform);
            dot.AddComponent<Image>().sprite = DotSprite;
            dot.GetComponent<Image>().SetNativeSize();
            dot.GetComponent<Image>().preserveAspect = true;
            dot.GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);
            dot.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, Random.Range(-15, 15));*/

            GameObject dot = Instantiate(ObjectPrefab, transform);
            dot.GetComponent<Image>().sprite = DotSprite;
            dot.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, Random.Range(-15, 15));

            //float scale = Random.Range(0.9f, 1.2f);
            //dot.GetComponent<RectTransform>().localScale = Vector3.one * scale;

            bool intersects = false;
            Vector2 pos = new Vector2();

            float tries = 0;
            do
            {
                pos.x = Random.Range(DotsArea.rect.xMin, DotsArea.rect.xMax);
                pos.y = Random.Range(DotsArea.rect.yMin, DotsArea.rect.yMax);

                dot.GetComponent<RectTransform>().anchoredPosition = pos;

                for (int j = 0; j < _dots.Count; j++)
                {
                    intersects = GetWorldSapceRect(dot.GetComponent<RectTransform>()).Overlaps(GetWorldSapceRect(_dots[j].GetComponent<RectTransform>()));
                    if (intersects)
                        break;
                }

                tries++;
            } while (intersects && tries < 1000);
            //print(tries);
            /*if (i == number - 1)
            {
                PotaTween.Create(dot).SetScale(Vector3.zero, Vector3.one * scale).SetAlpha(0f, 1f).SetEaseEquation(Ease.Equation.InOutBack).SetDuration(0.5f).SetDelay(i * 0.1f).Play(callback);
            }
            else
            {
                PotaTween.Create(dot).SetScale(Vector3.zero, Vector3.one * scale).SetAlpha(0f, 1f).SetEaseEquation(Ease.Equation.InOutBack).SetDuration(0.5f).SetDelay(i * 0.1f).Play();
            }*/

            _dots.Add(dot);
        }

        for (int i = 0; i < _dots.Count; i++)
        {
            GameObject dot = _dots[i];
            if (i == number - 1)
            {
                PotaTween.Create(dot).SetScale(Vector3.zero, dot.transform.localScale).SetAlpha(0f, 1f).SetEaseEquation(Ease.Equation.InOutBack).SetDuration(0.5f).SetDelay(i * 0.1f).Play(callback);
            }
            else
            {
                PotaTween.Create(dot).SetScale(Vector3.zero, dot.transform.localScale).SetAlpha(0f, 1f).SetEaseEquation(Ease.Equation.InOutBack).SetDuration(0.5f).SetDelay(i * 0.1f).Play();
            }
        }
        //callback();
    }

    Rect GetWorldSapceRect(RectTransform rt)
    {
        var r = rt.rect;
        r.center = rt.TransformPoint(r.center);
        r.size = rt.TransformVector(r.size);
        return r;
    }

    private void DestroyDots(System.Action callback = null)
    {
        if (_dots == null || _dots.Count == 0)
        {
            if (callback != null)
                callback();

            return;
        }

        for (int i = 0; i < _dots.Count; i++)
        {
            Destroy(_dots[i].gameObject);
            /*if (i == _dots.Count - 1)
            {
                PotaTween.Get(_dots[i]).SetDelay(0).Reverse(() => 
                {
                    Destroy(_dots[i].gameObject);
                    _dots = new List<GameObject>();

                    if (callback != null)
                        callback();
                });
            }
            else
            {
                PotaTween.Get(_dots[i]).SetDelay(0).Reverse(() =>
                {
                    Destroy(_dots[i].gameObject);
                });
            }*/
        }
        _dots = new List<GameObject>();

        if (callback != null)
            callback();
    }
}
