﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class NumberButton : MonoBehaviour
{
    [SerializeField]
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            if (Text != null)
                Text.text = _value.ToString();
        }
    }

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();

            return _text;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }   
}
