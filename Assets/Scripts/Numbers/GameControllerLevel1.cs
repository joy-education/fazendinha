﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public enum GameStep
{
    None,
    Dice,
    Operation,
    Space,
    Answer,
    EndGame,
    Report,
    SendMail
                
}

[System.Serializable]
public class Level1Toggles
{
    public List<NumberButton> Buttons;

    public void SetActive(bool active, System.Action callback = null)
    {        
        callback();
    }

    public void SetButtonValues(List<int> values, System.Action<int> buttonsCallback)
    {
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].Value = values[i];

            Buttons[i].Button.onClick.AddListener(() => 
            {
                buttonsCallback(Buttons[i].Value);
            });
        }
    }

    public void SetButton(int index, int value, System.Action<int> buttonCallback = null)
    {        
        Buttons[index].GetComponent<NumberButton>().Value = value;

        Buttons[index].Button.onClick.RemoveAllListeners();
        Buttons[index].Button.onClick.AddListener(() =>
        {
            buttonCallback(Buttons[index].Value);
        });
    }
}

public class GameControllerLevel1 : MonoBehaviour {

    public int PlayerIndex { get; set; }
    public int FirstNumber { get; set; }
    public int SecondNumber { get; set; }
    public Openable AnswerArea;
    public Level1AnswerSlot AnswerSlot;
    public Level1AnswerSlot AnswerSlot2;
    public List<Transform> NumberSlots;
    public Text AnswerText;
    public Text OperationText;
    public Text EqualSignText;

    public Level1Toggles NumberButtons = new Level1Toggles();
    
    public GamesFeedback Feedback;
    public Button AnswerButton;
    public Button EndGameButton;
    public Openable EndGameButtonPopup;
    public Button ReturnToMainMenuButton;
    public TextAnimation EndGameAnimation;

    public MathOperation CurrentOperation;
    public int NumberOfQuestions = 10;

    [Header("UI")]
    public Image Balloon;
    public Text Question;
    public Text QuestionsAnsweredText;
    public Openable Fader;

    [Header("Send Mail")]
    public ReportScroll EndGameScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;    

    private GameStep _currentStep = GameStep.None;
    private List<Level1DraggableNumber> _numbers;
    private int _playersFinished;

    private int _min = 1;
    private int _max = 10;

    private string _report;
    private int _rightAnswers;
    private int _wrongAnswers;
    private int _answeredQuestions;

    private void Awake()
    {
        _min = 1;
        _max = 10;
        _report = "";

        if (GameConfigs.MathOperation != MathOperation.None)
            CurrentOperation = GameConfigs.MathOperation;

        Fader.SetActive(false, () =>
        {
            SetQuestion();
            for (int i = 0; i < 10; i++)
            {
                NumberButtons.SetButton(i, i, (n) => 
                {
                    AudioController.PlaySFX(AudioController.SFXList.ButtonEnterMenuAnimation);                    
                    AudioController.PlayVoice(n.ToString());

                    if (AnswerText.text.Length < 3)
                        AnswerText.text += n.ToString();
                });
            }        
        });

    }

    private void Start()
    {
        AudioController.PlayMusic(AudioController.MusicList.MathGames);
    }

    public void SetQuestion()
    {
        _answeredQuestions++;
        
        if (CheckEndGame())
        {
            EndGame();
            return;
        }

        UpdateQuestionsAnsweredText();

        AnswerArea.SetActive(true);

        _currentStep = GameStep.None;
        
        switch (CurrentOperation)
        {
            case MathOperation.None:
                break;
            case MathOperation.Addition:
                OperationText.text = "+";
                break;
            case MathOperation.Subtraction:
                OperationText.text = "-";
                break;
            case MathOperation.Multiplication:
                OperationText.text = "x";
                break;
            case MathOperation.Division:
                OperationText.text = "/";
                break;
            default:
                break;
        }

        if (CurrentOperation != MathOperation.Subtraction)
        {
            FirstNumber = Random.Range(_min, _max);
            SecondNumber = Random.Range(_min, _max);
        }
        else
        {
            FirstNumber = Random.Range(2, _max);
            SecondNumber = Random.Range(1, FirstNumber);
        }

        List<int> answers = new List<int>() { FirstNumber };
        List<int> answers2 = new List<int>() { SecondNumber };

        AnswerSlot.SetActive(true, () =>
        {
            AnswerSlot.SetAnswerImage(FirstNumber, () =>
            {

            });
        });

        AnswerSlot2.SetActive(true, () =>
        {
            AnswerSlot2.SetAnswerImage(SecondNumber, () =>
            {
                    

            });
        });

        RegisterPlay($"Conta: {FirstNumber} {OperationText.text} {SecondNumber}\n");
    }

    private void UpdateQuestionsAnsweredText()
    {
        QuestionsAnsweredText.text = $"{_answeredQuestions}/{NumberOfQuestions}";
    }

    private void ShowFeedback()
    {
        _currentStep = GameStep.Answer;

        int answer;
        if (!int.TryParse(AnswerText.text, out answer))
        {
            Debug.LogError("NaN");
        }

        ClearAnswer();

        bool correct = answer == (GetAnswer());

        RegisterPlay("Resposta certa: " + (GetAnswer()) + "\n");
        RegisterPlay("Resposta dada: " + answer + "\n");

        if (correct)
        {
            RegisterPlay(" Correto!\n\n");
            print("CERTO");
            _rightAnswers++;

            Feedback.SetActive(true, "Correto!");

            AudioController.PlaySFX(AudioController.SFXList.RightAnswer);
            AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Correct);
        }
        else
        {
            RegisterPlay(" Errado...\n\n");
            print("ERRADO");
            _wrongAnswers++;

            Feedback.SetActive(true, "Errado...");

            AudioController.PlaySFX(AudioController.SFXList.WrongAnswer);
            AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Wrong);
        }

        AnswerArea.SetActive(false);

        Invoke("DisableFeedback", 2f);
    }

    private void DisableFeedback()
    {
        Feedback.SetActive(false, () => 
        {
            SetQuestion();
        });
    }

    private float GetAnswer()
    {
        switch (CurrentOperation)
        {
            default:
            case MathOperation.None:
            case MathOperation.Addition:
                return FirstNumber + SecondNumber;
            case MathOperation.Subtraction:
                return FirstNumber - SecondNumber;
            case MathOperation.Multiplication:
                return FirstNumber * SecondNumber;
            case MathOperation.Division:
                return FirstNumber / SecondNumber;
        }
    }

    private void RegisterPlay(string log)
    {
        _report += log;
    }

    public void OkButtonClicked()
    {
        AudioController.PlaySFX(AudioController.SFXList.MenuButtonPressed);

        switch (_currentStep)
        {
            case GameStep.None:
                EndGameButton.interactable = false;

                int result = 0;
                if (!int.TryParse(AnswerText.text, out result))
                {
                    return;
                }
                
                AnswerSlot.SetActive(false);
                AnswerSlot2.SetActive(false);
                NumberButtons.SetActive(false, () => 
                {
                    ShowFeedback();
                });
                break;
            case GameStep.Dice:
                break;
            case GameStep.Operation:
                break;
            case GameStep.Space:
                break;
            case GameStep.Answer:
                break;
            case GameStep.EndGame:
                break;
            case GameStep.Report:
                ReturnToMainMenu();
                break;
            case GameStep.SendMail:
                AnswerButton.gameObject.SetActive(false);
                
                break;
            default:
                break;
        }
    }

    public void ClearAnswer()
    {
        AudioController.PlaySFX(AudioController.SFXList.DrawingPressed);

        AnswerText.text = "";
    }

    public void EndGameButtonClicked()
    {
        AudioController.PlaySFX(AudioController.SFXList.MenuButtonPressed);

        EndGameButton.interactable = false;

        AnswerSlot.SetActive(false);
        AnswerSlot2.SetActive(false);
        NumberButtons.SetActive(false, () =>
        {
            ResetAll();

            EndGame();
        });
        
    }

    private void ResetAll()
    {

        AnswerSlot.SetActive(false);
        AnswerSlot2.SetActive(false);
        NumberButtons.SetActive(false, () =>
        {
            //ShowFeedback();
        });
    }

    private bool CheckEndGame()
    {
        if (_answeredQuestions <= NumberOfQuestions)
            return false;

        return true;
    }

    private void Report()
    {
        _currentStep = GameStep.Report;

        AnswerButton.gameObject.SetActive(false);

        EndGameScroll.SetActive(true);

        _report += "\nRespostas Certas: " + _rightAnswers;
        _report += "\nRespostas Erradas: " + _wrongAnswers;

        EndGameScroll.ContentText.text = _report;
    }

    private void EndGame()
    {
        _currentStep = GameStep.EndGame;

        ResetAll();

        QuestionsAnsweredText.gameObject.SetActive(false);

        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Congratz, 1f);
        EndGameAnimation.SetActive(true, () => 
        {
            EndGameAnimation.SetActive(false, () => 
            {
                Report();
            });
        });
    }

    public void ReturnToMainMenu()
    {
        AudioController.StopMusic();
        AudioController.PlaySFX(AudioController.SFXList.MenuButtonPressed);

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Back;
        AudioController.PlayVoice(voice);
        Fader.SetActive(true);
        CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () => 
        {
            SceneManager.LoadScene("MainMenu");
        });
    }
}
