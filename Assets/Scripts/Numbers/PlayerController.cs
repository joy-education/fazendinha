﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerColor
{
    Red,
    Green,
    Blue,
    Yellow
}

public class PlayerController : MonoBehaviour {

    public PlayerColor Color;
    public string Log = "";
    public Text NameText;

    public float TotalScore { get; set; }
    public float TotalRightAnswers { get; set; }
    public float TotalWrongAnswers { get; set; }
    public bool FinishedGame { get; set; }

    public string Name
    {
        get
        {
            return NameText.text;
        }
        set
        {
            NameText.text = value;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void FadeIn(System.Action callback = null)
    {
        gameObject.SetActive(true);

        Tween.Stop();
        Tween.Play(callback);
    }

    public void FadeOut(System.Action callback = null)
    {
        Tween.Stop();
        Tween.Reverse(() => 
        {
            if (callback != null)
                callback();

            gameObject.SetActive(false);
        });
    }


}
