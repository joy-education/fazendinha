﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class Level1DraggableNumber : DraggableElement
{

    public Level1AnswerSlot CollidingSlot
    {
        get
        {
            float distance = Mathf.Infinity;
            Level1AnswerSlot slot = null;
            for (int i = 0; i < _collidingSlots.Count; i++)
            {
                float dist = Vector3.Distance(transform.position, _collidingSlots[i].transform.position);
                if (dist < distance)
                {
                    slot = _collidingSlots[i];
                    distance = dist;
                }
            }

            return slot;
        }
    }

    private List<Level1AnswerSlot> _collidingSlots = new List<Level1AnswerSlot>();

    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            TextMesh.text = _value.ToString();
        }
    }

    private TextMeshProUGUI _textMesh;
    public TextMeshProUGUI TextMesh
    {
        get
        {
            if (_textMesh == null)
                _textMesh = GetComponentInChildren<TextMeshProUGUI>();

            return _textMesh;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Level1AnswerSlot slot = collision.GetComponent<Level1AnswerSlot>();
        if (slot != null)
        {
            _collidingSlots.Add(slot);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Level1AnswerSlot slot = collision.GetComponent<Level1AnswerSlot>();
        if (slot != null)
        {
            _collidingSlots.Remove(slot);
        }
    }
}
