﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumericKeyboard : MonoBehaviour {

    public Text Display;
    public List<Button> NumericButtons;
    public Button DecimalSeparatorButton;
    public Button MinusButton;
    public Button DeleteButton;

    private string _value;
    public string Value
    {
        get
        {
            if (string.IsNullOrEmpty(_value))
                _value = string.Empty;
            return _value;
        }
        set
        {
            _value = value;
            Display.text = _value;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            return _tween;
        }
    }

    private void Awake()
    {
        for (int i = 0; i < NumericButtons.Count; i++)
        {
            AssignButtonValue(NumericButtons[i], i.ToString());
        }
        AssignButtonValue(DecimalSeparatorButton, ",");
        AssignButtonValue(MinusButton, "-");

        DeleteButton.onClick.AddListener(DeleteButtonClicked);

        EnableButtons();
    }

    private void AssignButtonValue(Button button, string value)
    {
        button.onClick.AddListener(() => ButtonClicked(value));
    }

    private void ButtonClicked(string buttonValue)
    {
        Value += buttonValue;
        EnableButtons();

        //VoicePlayer.Instance.Play(buttonValue);
    }

    private void DeleteButtonClicked()
    {
        if (Value.Length > 0)
            Value = Value.Substring(0, Value.Length - 1);
        EnableButtons();
        //VoicePlayer.Instance.Play("apagar");
    }

    private void EnableButtons()
    {
        MinusButton.interactable = string.IsNullOrEmpty(Value);
        DecimalSeparatorButton.interactable = !Value.Contains(",") && !string.IsNullOrEmpty(Value) && (Value.Length > 0 && Value[Value.Length-1] != '-');
    }

    public void Reset()
    {
        Value = string.Empty;
        Display.text = "?";
    }

    public void SetActive(bool active)
    {
        Tween.Stop();
        if (active)
        {
            gameObject.SetActive(active);
            Tween.Play();
            EnableButtons();
        }
        else
        {
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
