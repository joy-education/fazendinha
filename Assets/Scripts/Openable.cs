﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Openable : MonoBehaviour {
    [System.Serializable]
    public class OpenableEvent : UnityEvent { }

    public OpenableEvent OnOpening;
    public OpenableEvent OnOpened;
    public OpenableEvent OnClosing;
    public OpenableEvent OnClosed;

    public bool IsActive
    {
        get
        {
            return gameObject.activeSelf;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Get(gameObject, 0);
            if (_tween == null)
                _tween = PotaTween.Create(gameObject, 0).SetAlpha(0f, 1f).SetDuration(0.3f);

            return _tween;
        }
    }

    private PotaTween _closeTween;
    public PotaTween CloseTween
    {
        get
        {
            if (_closeTween == null)
                _closeTween = PotaTween.Get(gameObject, 1);

            return _closeTween;
        }
    }

    public void SetActive(bool active)
    {
        SetActive(active, null);
    }

    public void SetActive(bool active, System.Action callback)
    {
        Tween.Stop();

        if (active)
        {
            gameObject.SetActive(true);
            OnOpening.Invoke();
            Tween.Play(() => 
            {
                OnOpened.Invoke();

                if (callback != null)
                    callback();
            });
        }
        else
        {
            gameObject.SetActive(true);
            OnClosing.Invoke();

            System.Action closedCallback = () =>
            {
                if (callback != null)
                    callback();

                OnClosed.Invoke();
                gameObject.SetActive(false);
            };

            if (CloseTween == null)
                Tween.Reverse(closedCallback);
            else
                CloseTween.Play(closedCallback);
        }
    }
}
