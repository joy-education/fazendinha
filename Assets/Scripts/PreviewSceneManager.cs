﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PreviewSceneManager : MonoBehaviour
{
    public Openable Fader;
    public ScrollRect ScrollView;
    public HoldButon ScrollDownButton;
    public HoldButon ScrollUpButton;

    private void Awake()
    {
        Fader.SetActive(false);
        //ScrollView.horizontalNormalizedPosition = 0;
    }

    private void Start()
    {
        AudioController.PlayMusic(AudioController.MusicList.LettersMode);
        ScrollView.horizontalNormalizedPosition = 0;

        ScrollDownButton.onPressed.AddListener(() => 
        {
            Scroll(50);
        });

        ScrollUpButton.onPressed.AddListener(() => 
        {
            Scroll(-50);
        });
    }

    public void Scroll(float value)
    {
        //ScrollView.horizontalNormalizedPosition = Mathf.Clamp01(ScrollView.horizontalNormalizedPosition + value);
        ScrollView.velocity += new Vector2(value, 0);
    }

    public void BackToMainMenu()
    {
        AudioController.StopMusic();
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Back;
        AudioController.PlayVoice(voice);
        Fader.SetActive(true); 
        CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () => 
        {
            SceneManager.LoadScene("MainMenu");
        });
    }
}
