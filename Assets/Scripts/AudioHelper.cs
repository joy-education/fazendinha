﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelper : MonoBehaviour
{
    public void PlaySFX(AudioClip clip)
    {
        AudioController.PlaySFX(clip);
    }
}
