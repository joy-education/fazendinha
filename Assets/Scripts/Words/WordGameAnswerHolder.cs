﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordGameAnswerHolder : MonoBehaviour {

    public List<LetterSlot> LetterSlots;
    public RectTransform SlotsContainer;

    public string LettersMissing
    {
        get
        {
            string letters = string.Empty;

            for (int i = 0; i < LetterSlots.Count; i++)
            {
                if (LetterSlots[i].IsFilled)
                    continue;

                letters += LetterSlots[i].Value;
            }

            return letters.ToLower();
        }
    }

    public void CreateSlots(string word, LettersToComplete lettersToComplete)
    {
        ClearSlots();

        for (int i = 0; i < word.Length; i++)
        {            
            LetterSlot slot = Instantiate(WordGameController.Instance.LetterSlotPrefab);
            slot.Value = word[i].ToString();
            FillSlot(i, slot, lettersToComplete);

            slot.transform.SetParent(SlotsContainer);

            LetterSlots.Add(slot);

            float alphaTo = 1f;
            if (slot.Value == " ")
                alphaTo = 0.001f;

            PotaTween.Create(slot.gameObject).
                SetScale(Vector3.zero, Vector3.one).
                SetAlpha(0f, alphaTo).
                SetEaseEquation(Ease.Equation.OutBack).
                SetDuration(0.5f).
                SetDelay(0.2f * i).
                Play();

            Invoke("PlaySFX", 0.2f * i);
        }
    }

    private void PlaySFX()
    {
        AudioController.PlaySFX(AudioController.SFXList.DrawingPressed);
    }

    public void ClearSlots()
    {
        for (int i = 0; i < LetterSlots.Count; i++)
        {
            LetterSlots[i].DestroySelf();// 0.2f * i);
        }

        LetterSlots = new List<LetterSlot>();
    }

    private void FillSlot(int slotIndex, LetterSlot slot, LettersToComplete lettersToComplete)
    {
        string word = WordGameController.Instance.Word.Name;

        if (" -".Contains(slot.Value))
        {
            CreateLetterOnSlot(slot);
            return;
        }

        switch (lettersToComplete)
        {
            case LettersToComplete.None:
                break;
            case LettersToComplete.Vowels:
                if (!"aeiouAEIOU".Contains(slot.Value))
                    CreateLetterOnSlot(slot);
                break;
            case LettersToComplete.Consonants:
                if ("aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(slot.Value))
                    CreateLetterOnSlot(slot);
                break;
            case LettersToComplete.Encounters:
                if ((slotIndex < word.Length - 1 && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(slot.Value) && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(word[slotIndex + 1].ToString()))
                    || (slotIndex < word.Length - 1 && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(slot.Value) && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(word[slotIndex + 1].ToString()))
                    || (slotIndex > 0 && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(slot.Value) && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(word[slotIndex - 1].ToString()))
                    || (slotIndex > 0 && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(slot.Value) && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(word[slotIndex - 1].ToString())))
                    break;
                else
                    CreateLetterOnSlot(slot);
                break;
            case LettersToComplete.All:
                break;
            default:
                break;
        }
    }

    private void CreateLetterOnSlot(LetterSlot slot)
    {
        slot.Collider.enabled = false;
        slot.IsFilled = true;
        DraggableLetter letter = Instantiate(WordGameController.Instance.LetterPrefab, slot.transform);
        letter.Interactable = false;
        letter.Value = slot.Value;
        StartCoroutine(WaitToSetLetterScale(letter, slot));
        letter.RectTransform.anchoredPosition = Vector2.zero;
    }

    IEnumerator WaitToSetLetterScale(DraggableLetter letter, LetterSlot slot)
    {
        yield return new WaitForEndOfFrame();
        slot.transform.localScale = Vector3.one;
        float scale = slot.GetComponent<RectTransform>().sizeDelta.x / (slot.GetComponent<Image>().sprite.bounds.size.x * 100);
        scale = Mathf.Clamp01(scale);
        letter.transform.localScale *= scale;
    }
}
