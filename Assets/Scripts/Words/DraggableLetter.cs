﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DraggableLetter : DraggableElement, IPointerDownHandler
{

    private string _value;
    public string Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value.ToUpper();
            TextRenderer.text = _value;
        }
    }

    private Text _textRenderer;
    public Text TextRenderer
    {
        get
        {
            if (_textRenderer == null)
                _textRenderer = GetComponent<Text>();
            return _textRenderer;
        }
    }

    private RectTransform _rectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }

    private Collider2D _collider;
    public Collider2D Collider
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<Collider2D>();
            return _collider;
        }
    }

    private LetterSlot _slot;
	public LetterSlot CollidingSlot
    {
        get
        {
            if (!Collider.enabled)
                return _slot;

            LetterSlot slot = null;
            float distance = Mathf.Infinity;
            for (int i = 0; i < _collidingSlots.Count; i++)
            {
                float dist = Vector2.Distance(RectTransform.position, _collidingSlots[i].GetComponent<RectTransform>().position);
                if (dist < distance)
                {
                    distance = dist;
                    slot = _collidingSlots[i];
                }
            }
            _slot = slot;

            return _slot;
        }
    }
    
    private bool _interactable;
    public bool Interactable
    {
        get
        {
            return _interactable;
        }
        set
        {
            _interactable = value;
            Collider.enabled = _interactable;
            TextRenderer.raycastTarget = _interactable;
        }
    }

    private List<LetterSlot> _collidingSlots = new List<LetterSlot>();

    [SerializeField]
    public DragEvent OnClick;

    private void OnTriggerEnter2D(Collider2D col)
    {
        LetterSlot slot = col.gameObject.GetComponent<LetterSlot>();

        if (slot == null)
            return;

        _collidingSlots.Add(slot);
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        LetterSlot slot = col.gameObject.GetComponent<LetterSlot>();

        if (slot == null)
            return;

        _collidingSlots.Remove(slot);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnClick.Invoke(this);
    }
}
