﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordGameLetterHolderTray : MonoBehaviour {

    public List<WordGameLetterHolder> List;
    public int NumberOfSlots = 5;
    public bool CreateSlotsOnAwake = false;

    private void Awake()
    {
        /*switch (WordGameController.Instance.LettersToComplete)
        {
            case LettersToComplete.None:
                break;
            case LettersToComplete.Vowels:
                NumberOfSlots = 5;
                break;
            case LettersToComplete.Consonants:
                NumberOfSlots = 8;
                break;
            case LettersToComplete.Encounters:
                NumberOfSlots = 8;
                break;
            case LettersToComplete.All:
                NumberOfSlots = 10;
                break;
            default:
                break;
        }*/

        for (int i = 0; i < NumberOfSlots; i++)
        {
            WordGameLetterHolder letter = Instantiate(WordGameController.Instance.LetterHolderPrefab, transform);

            switch (i)
            {
                default:
                    break;
                case 0:
                    letter.Value = "A";
                    break;
                case 1:
                    letter.Value = "E";
                    break;
                case 2:
                    letter.Value = "I";
                    break;
                case 3:
                    letter.Value = "O";
                    break;
                case 4:
                    letter.Value = "U";
                    break;
            }

            List.Add(letter);
        }

        Invoke("DisableLayoutGroup", 0.1f);
    }

    void DisableLayoutGroup()
    {
        HorizontalLayoutGroup group = GetComponent<HorizontalLayoutGroup>();
        group.enabled = false;
    }
}
