﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum LettersToComplete
{
    None,
    Vowels,
    Consonants,
    Encounters,
    All
}

public class WordGameController : MonoBehaviour {

    public TSVReader TSVReader;
    public Word Word;
    public int ChanceOfWordLetter = 75;
    public int MaxNumberOfWords = 10;
    public Text HeaderText;
    public Text WordCountText;
    public Openable Fader;
    public WordGameAnswerHolder AnswerHolder;
    public WordGameHint Hint;
    public WordGameLetterHolderTray Holders;
    public Transform DraggingLettersHolder;
    public WordGameLetterSpawner LetterSpawner;
    public LettersToComplete LettersToComplete;

    [Header("Prefabs")]
    public DraggableLetter LetterPrefab;
    public WordGameLetterHolder LetterHolderPrefab;
    public LetterSlot LetterSlotPrefab;

    [Header("Report")]
    public ReportScroll ReportScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;
    [SerializeField]
    private Color _rightColor = Color.green;
    public string RightColor
    {
        get
        {
            return ColorUtility.ToHtmlStringRGBA(_rightColor);
        }
    }

    [SerializeField]
    private Color _wrongColor = Color.red;
    public string WrongColor
    {
        get
        {
            return ColorUtility.ToHtmlStringRGBA(_wrongColor);
        }
    }

    private List<Sprite> _wordSprites = new List<Sprite>();
    private List<Word> _words = new List<Word>();
    private List<Word> _fullWords = new List<Word>();

    private int _wordsCompleted;

    private string _report;
    public string Log
    {
        get
        {
            return _report;
        }
    }

    private int _wrongAnswers;
    private int _rightAnswers;
    private int _wordWrongAnswers;
    private int _wordRightAnswers;

    private System.DateTime _questionStartTime;
    private System.DateTime _questionEndTime;
    private System.TimeSpan _gameTotalTime;
    private System.DateTime _gameStartTime;
    private System.DateTime _gameEndTime;

    public List<WordGameLetterHolder> LetterHolders
    {
        get
        {
            return Holders.List;//new List<SeaLetter>(FindObjectsOfType<SeaLetter>());
        }
    }

    private static WordGameController _instance;
    public static WordGameController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<WordGameController>();

            return _instance;
        }
    }

    private void Awake()
    {
        Fader.SetActive(false, () => 
        {
            StartGame();
        });
    }

    void Start()
    {
        HeaderText.text = "DIGITE O NOME DO JOGADOR";

        if (GameConfigs.LettersToComplete != LettersToComplete.None)
            LettersToComplete = GameConfigs.LettersToComplete;

        //_wordSprites = new List<Sprite>(Resources.LoadAll<Sprite>("SeaOfLetters"));

        _words = TSVReader.Words;
        _fullWords = _words;

        //StartGame();

        AudioController.PlayMusic(AudioController.MusicList.WordsGames);
    }

	public void StartGame()
    {
        //LogPlay("Nível " + GameConfigs.GameLevel + "\n" + GameConfigs.PlayerName + "\n");

        AnswerHolder.gameObject.SetActive(true);
        PotaTween.Get(AnswerHolder.gameObject, 1).Play(() => 
        {
            Hint.gameObject.SetActive(true);
            GetNewWord();
        });

        Holders.gameObject.SetActive(true);

        /*if (LettersToComplete == LettersToComplete.All)
        {
            for (int i = 0; i < Holders.List.Count; i++)
            {
                Destroy(Holders.List[i].gameObject);
            }
            Holders.List = new List<WordGameLetter>();

            LetterSpawner.StartSpawning();
        }*/

        _gameStartTime = System.DateTime.Now;
        WordCountText.text = $"{_wordsCompleted.ToString("00")}/{MaxNumberOfWords.ToString("00")}";
    }

    public void CheckWordCompletion()
    {
        for (int i = 0; i < AnswerHolder.LetterSlots.Count; i++)
        {
            if (!AnswerHolder.LetterSlots[i].IsFilled)
                return;
        }
        _wordsCompleted++;
        //AnswerHolder.ClearSlots();

        _questionEndTime = System.DateTime.Now;
        System.TimeSpan time = _questionEndTime.Subtract(_questionStartTime);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)time.TotalMinutes,
        time.Seconds);
        LogPlay("\nTempo de resposta: " + t);

        HeaderText.text = "CORRETO!";
        WordCountText.text = _wordsCompleted.ToString() + "/10";

        LogPlay("\nTentativas corretas: " + _wordRightAnswers.ToString() + "\nTentativas erradas: " + _wordWrongAnswers.ToString() + "\n");

        //AudioController.PlaySFX(AudioController.SFXList.RightAnswer);
        AudioController.PlayVoice(AudioController.VoiceList.CurrentLocalization.Congratz);

        if (!CheckEndGame())
        {

            //Invoke("GetNewWord", 1f);
            CoroutineManager.WaitForSeconds(2f, () => 
            {
                AnswerHolder.ClearSlots();
                CoroutineManager.WaitForSeconds(1f, () => 
                {
                    GetNewWord();
                });
            });
        }
    }

    public bool CheckEndGame()
    {
        if (_wordsCompleted >= MaxNumberOfWords)
        {
            EndGame();
            return true;
        }
        return false;
    }

    public void GetNewWord()
    {
        HeaderText.text = "COMPLETE A PALAVRA";

        //Sprite sprite = _wordSprites[Random.Range(0, _wordSprites.Count)];
        //Word = sprite.name;
        Word = _words[Random.Range(0, _words.Count)];

        _words.Remove(Word);
        if (_words.Count <= 0)
            _words = _fullWords;
        
        AnswerHolder.CreateSlots(Word.Name, LettersToComplete);
        Hint.SetImage(Word.Sprite);

        if(LettersToComplete != LettersToComplete.Vowels)
        {
            List<WordGameLetterHolder> list = new List<WordGameLetterHolder>(LetterHolders);

            int n = list.Count;
            System.Random rng = new System.Random();
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                WordGameLetterHolder value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            for (int i = 0; i < list.Count; i++)
                list[i].Value = string.Empty;
            for (int i = 0; i < list.Count; i++)
                list[i].CreateNewLetter(false);
        }

        _wordWrongAnswers = 0;
        _wordRightAnswers = 0;

        LogPlay("\nPalavra: " + Word.Name.ToUpper() + "\n");

        _questionStartTime = System.DateTime.Now;
    }

    public string GetNewLetter()
    {
        switch (LettersToComplete)
        {
            case LettersToComplete.None:
            case LettersToComplete.All:
            default:
                return GetAnyLetter();
            case LettersToComplete.Vowels:
                return GetAnyLetter("aeiou");
            case LettersToComplete.Consonants:
                return GetAnyLetter("bcdfghjklmnpqrstvwxyz");
            case LettersToComplete.Encounters:
                return GetAnyLetter();
        }
    }    
    
    private string GetNewEncounter()
    {
        return null;
    }

    private string GetSimilarLetters(string l = "abcdefghijklmnopqrstuvwxyzçáãéíóõú")
    {
        string letters = string.Empty;

        string word = Word.Name;

        if (LettersToComplete == LettersToComplete.Consonants || LettersToComplete == LettersToComplete.All)
        {
            if (word.Contains("ca") || word.Contains("co") || word.Contains("cu"))
                letters += "kq";
            if (word.Contains("ce") || word.Contains("ci"))
                letters += "s";
            if (word.Contains("f"))
                letters += "v";
            if (word.Contains("k"))
                letters += "cq";
            if (word.Contains("l"))
                letters += "u";
            if (word.Contains("q"))
                letters += "ck";
            if (word.Contains("s"))
                letters += "cçz";
            if (word.Contains("w"))
                letters += "uv";
            if (word.Contains("x"))
                letters += "ch";
            if (word.Contains("y"))
                letters += "i";
            if (word.Contains("z"))
                letters += "s";
            if (word.Contains("ç"))
                letters += "cs";
            if (word.Contains("m"))
                letters += "n";
        }
        else if (LettersToComplete == LettersToComplete.Encounters)
        {
            if (word.Contains("ch"))
                letters += "x";
            if (word.Contains("ss"))
                letters += "ç";
            if (word.Contains("sc"))
                letters += "ç";
            if (word.Contains("m"))
                letters += "n";
        }
        if (/*LettersToComplete == LettersToComplete.Encounters || */LettersToComplete == LettersToComplete.All)
        {
            if (word.Contains("a"))
                letters += "áã";
            if (word.Contains("á"))
                letters += "aã";
            if (word.Contains("ã"))
                letters += "aá";
            if (word.Contains("e"))
                letters += "é";
            if (word.Contains("é"))
                letters += "e";
            if (word.Contains("i"))
                letters += "í";
            if (word.Contains("í"))
                letters += "i";
            if (word.Contains("o"))
                letters += "óõ";
            if (word.Contains("ó"))
                letters += "oõ";
            if (word.Contains("õ"))
                letters += "oó";
            if (word.Contains("u"))
                letters += "ú";
            if (word.Contains("ú"))
                letters += "u";
        }

        string let = string.Empty;
        foreach (var c in letters)
        {
            if (let.IndexOf(c) == -1)
                let += c;
        }
        letters = let;

        for (int i = 0; i < LetterHolders.Count; i++)
        {
            if (!string.IsNullOrEmpty(LetterHolders[i].Value))
                l.Replace(LetterHolders[i].Value.ToLower(), "");
        }

        string missingLetters = AnswerHolder.LettersMissing;
        string ml = string.Empty;
        foreach (var c in missingLetters)
        {
            if (ml.IndexOf(c) == -1)
                ml += c;
        }
        missingLetters = ml;

        return letters;
    }

    private string GetAnyLetter(string letters = "abcdefghijklmnopqrstuvwxyzçáãéíóõú")
    {
        string l = GetSimilarLetters(letters);

        string lettersMissing = AnswerHolder.LettersMissing;

        bool anyLetterExists = false;
        List<WordGameLetterHolder> holders = new List<WordGameLetterHolder>(LetterHolders);
        for (int i = 0; i < holders.Count; i++)
        {
            if (string.IsNullOrEmpty(holders[i].Value.ToLower()))
                continue;

            if (lettersMissing.Contains(holders[i].Value.ToLower()))
            {
                //lettersMissing = lettersMissing.Replace(LetterHolders[i].Value.ToLower(), "");                
                lettersMissing = lettersMissing.Remove(lettersMissing.LastIndexOf(holders[i].Value.ToLower()), 1);

                anyLetterExists = true;
            }
            
            l = l.Replace(holders[i].Value.ToLower(), "");
        }

        if (l.Length <= 0)
        {
            for (int i = lettersMissing.Length; i < 5; i++)
            {
                int letterIndex = Random.Range(0, letters.Length);
                l += letters[letterIndex];
                letters.Replace(letters[letterIndex].ToString(), string.Empty);
            }
        }

        int probWordLetter = Random.Range(0, 100);
        if ((anyLetterExists == false || probWordLetter <= ChanceOfWordLetter) && !string.IsNullOrEmpty(lettersMissing))
            return lettersMissing[Random.Range(0, lettersMissing.Length)].ToString();
        else
            return l[Random.Range(0, l.Length)].ToString();
    }

    public void LetterReleased(WordGameLetterHolder letter)
    {
        if(letter.Letter.CollidingSlot != null)
        {
            string color = string.Empty;
            string l = letter.Value;
            LetterSlot slot = letter.Letter.CollidingSlot;
            if (letter.Letter.CollidingSlot.Value.ToLower() == letter.Value.ToLower())
            {
                letter.CorrectSlot();// () => CheckWordCompletion());
                CheckWordCompletion();
                color = RightColor;
                _rightAnswers++;
                _wordRightAnswers++;

                AudioController.PlaySFX(AudioController.SFXList.RightAnswer);
            }
            else
            {
                letter.ReturnToPosition();
                color = WrongColor;
                _wrongAnswers++;
                _wordWrongAnswers++;

                AudioController.PlaySFX(AudioController.SFXList.WrongAnswer);
            }
            for (int i = 0; i < AnswerHolder.LetterSlots.Count; i++)
            {
                if (AnswerHolder.LetterSlots[i] == slot)
                {
                    LogPlay("<color=#" + color + ">" + l.ToUpper() + "</color>");                
                }
                else if (AnswerHolder.LetterSlots[i].IsFilled)
                    LogPlay(AnswerHolder.LetterSlots[i].Value.ToUpper());
                else
                    LogPlay("_");
            }
            LogPlay("\n");
            print(_report);
            //CheckWordCompletion();
        }
        else
        {
            letter.ReturnToPosition();
        }
    }

    private void EndGame()
    {
        //BackToMainMenu();
        PotaTween.Get(AnswerHolder.gameObject, 1).Reverse(() => { AnswerHolder.gameObject.SetActive(false); });
        if (Holders.gameObject.activeSelf)
            Holders.gameObject.SetActive(false);
        LetterSpawner.gameObject.SetActive(false);
        Hint.gameObject.SetActive(false);

        _gameEndTime = System.DateTime.Now;
        _gameTotalTime = _gameEndTime.Subtract(_gameStartTime);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)_gameTotalTime.TotalMinutes,
        _gameTotalTime.Seconds);

        LogPlay("\n\nTempo total da atividade: " + t + "\n");
        LogPlay("Total de tentativas corretas: " + _rightAnswers + "\nTotal de tentativas erradas: " + _wrongAnswers);

        ReportScroll.ContentText.text = _report;
        ReportScroll.SetActive(true);
    }

    public void BackToMainMenu()
    {
        AudioController.StopMusic();
        AudioController.PlaySFX(AudioController.SFXList.MenuButtonPressed);

        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Back;
        AudioController.PlayVoice(voice);
        Fader.SetActive(true);
        CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () => 
        {
            SceneManager.LoadScene("MainMenu");
        });
    }

    public void LogPlay(string log)
    {
        _report += log;
    }
    
}
