﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayAgainButton : MonoBehaviour
{
    public Openable Fader;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            AudioClip voice = AudioController.VoiceList.CurrentLocalization.PlayAgain;
            AudioController.PlayVoice(voice);
            Fader.SetActive(true);
            CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () => 
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            });
        });
    }
}
