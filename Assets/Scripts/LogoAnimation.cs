﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LogoAnimation : MonoBehaviour
{
    [System.Serializable]
    public class LogoEvent : UnityEvent { }

    public ntw.CurvedTextMeshPro.TextProOnACircle LogoText;
    public float TextTweenDuration = 1.5f;
    public float TextTweenDelay = 0.5f;
    public Ease.Equation TextTweenEaseEquation = Ease.Equation.OutBack;
    public Vector2 TextOffsetFromTo = new Vector2(-180, -80);
    public Vector2 TextArcFromTo = new Vector2(75, 52);

    public Image LogoImage;

    public LogoEvent OnAnimationEnd;

    private PotaTween _textOffsetTween;
    public PotaTween TextOffsetTween
    {
        get
        {
            if (_textOffsetTween == null)
                _textOffsetTween = PotaTween.Create(gameObject).
                    SetFloat(TextOffsetFromTo.x, TextOffsetFromTo.y).
                    SetDelay(TextTweenDelay).
                    SetDuration(TextTweenDuration).
                    SetEaseEquation(TextTweenEaseEquation).
                    UpdateCallback(UpdateTextOffset);

            return _textOffsetTween;
        }
    }

    private PotaTween _textArcTween;
    public PotaTween TextArcTween
    {
        get
        {
            if (_textArcTween == null)
                _textArcTween = PotaTween.Create(gameObject, 1).
                    SetAlpha(0f, 5f).
                    SetFloat(TextArcFromTo.x, TextArcFromTo.y).
                    SetDelay(TextTweenDelay).
                    SetDuration(TextTweenDuration).
                    SetEaseEquation(TextTweenEaseEquation).
                    UpdateCallback(UpdateTextArc);

            return _textArcTween;
        }
    }

    private PotaTween _logoImgTween;
    public PotaTween LogoImgTween
    {
        get
        {
            if (_logoImgTween == null)
                _logoImgTween = PotaTween.Create(LogoImage.gameObject).
                    SetDuration(2f).
                    SetPosition(TweenAxis.Y, 1200, -125, true).
                    SetEaseEquation(Ease.Equation.OutBounce);

            return _logoImgTween;
        }
    }

    public static bool HasPlayed;

    public void Play()
    {
        Play(null);
    }

    public void Play(System.Action callback)
    {        
        LogoText.gameObject.SetActive(true);
        TextOffsetTween.Play(() => 
        {
            LogoImage.gameObject.SetActive(true);
            LogoImgTween.Play(() =>
            {
                HasPlayed = true;
                OnAnimationEnd.Invoke();
                callback?.Invoke();
            });
        });
        TextArcTween.Play();
    }

    public void UpdateTextOffset()
    {
        LogoText.SetAngularOffset(TextOffsetTween.Float.Value);
    }

    public void UpdateTextArc()
    {
        LogoText.SetArcDegrees(TextArcTween.Float.Value);
    }
}
