﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreviewButton : MonoBehaviour
{
    public string Value
    {
        get
        {
            return Text.text;
        }
        set
        {
            Text.text = value;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();

            return _text;
        }
    }

    private RectTransform _rectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = transform as RectTransform;
            return _rectTransform;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            AudioController.PlayVoice(Value);
        });

        Vector3 rot = RectTransform.eulerAngles;
        rot.z = Random.Range(-10, 10);
        RectTransform.eulerAngles = rot;
    }
}
