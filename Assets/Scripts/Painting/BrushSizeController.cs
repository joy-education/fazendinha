﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushSizeController : MonoBehaviour
{
    public TexturePainter PainterManager;

    public void SetBrushSize(BrushSizeToggle toggle)
    {
        if (toggle.Toggle.isOn)
        {
            PainterManager.SetBrushSize(toggle.Size);
        }
        AudioController.PlaySFX(AudioController.SFXList.BrushSizePressed);
    }
}
