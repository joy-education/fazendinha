﻿/// <summary>
/// CodeArtist.mx 2015
/// This is the main class of the project, its in charge of raycasting to a model and place brush prefabs infront of the canvas camera.
/// If you are interested in saving the painted texture you can use the method at the end and should save it to a file.
/// </summary>

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TexturePainter : MonoBehaviour
{
    public enum PaintMode
    {
        Brush,
        Fill
    }

    public PaintMode Mode = PaintMode.Fill;
	public GameObject BrushCursor, BrushContainer; //The cursor that overlaps the model and our container for the brushes painted
	public Camera SceneCamera, CanvasCam;  //The camera that looks at the model, and the camera that looks at the canvas.
	public Sprite CursorPaint; // Cursor for the differen functions 
	public RenderTexture CanvasTexture; // Render Texture that looks at our Base Texture and the painted brushes
	public Material CanvasMaterial; // The material of our base texture (Were we will save the painted texture)
	public Material BaseMaterial; // The material of our base texture (Were we will save the painted texture)
	public float BrushSize = 1.0f; //The size of our brush
    public float BrushSpacing = 0.1f; //Spacing between brushes
	public Color BrushColor; //The selected color
    public GameObject BrushPrefab;
    public GameObject CanvasObject;

    private int _brushCounter = 0;
    private Vector3 _lastCursorPosition;
    private Texture2D _canvasTexture;

    public Material CanvasInstanceMaterial
    {
        get
        {
            return CanvasObject.GetComponent<Renderer>().material;
        }
    }

    private const int MAX_BRUSH_COUNT = 500; //To avoid having millions of brushes

    private void Awake()
    {
        if (Mode == PaintMode.Brush)
        {
            if (GameConfigs.PaintingOutline != null)
            BaseMaterial.SetTexture("_Outline", GameConfigs.PaintingOutline);
        }

        //BaseMaterial.mainTexture = null;
    }

    void Update()
    {
        if (Mode == PaintMode.Brush)
        {
            //brushColor = ColorSelector.GetColor ();	//Updates our painted color with the selected color

            if (Input.GetMouseButtonDown(0))
                if (HitTestUVPosition(ref _lastCursorPosition))
                    CreateBrush(_lastCursorPosition);

            if (Input.GetMouseButton(0))
                Paint();

            if (Input.GetMouseButtonUp(0))
                SaveTexture();
        }
        else
        {
            if (Input.GetMouseButton(0))
			    Fill();
        }
    }

    public void SetOutline(Texture2D outline)
    {
        if (Mode == PaintMode.Fill)
        {
            CanvasInstanceMaterial.SetTexture("_Outline", outline);

            Clear();
        }
        else
        {
            CanvasMaterial.SetTexture("_Outline", outline);
        }
    }

    public void Clear()
    {
        Texture2D outline = CanvasInstanceMaterial.GetTexture("_Outline") as Texture2D;

        //CanvasInstanceMaterial.mainTexture = null;

        _canvasTexture = new Texture2D(outline.width, outline.height);
        _canvasTexture.SetPixels(outline.GetPixels());
        _canvasTexture.Apply();
        CanvasInstanceMaterial.mainTexture = _canvasTexture;
        
    }

    void Fill()
    {
        Vector3 uvWorldPosition = Vector3.zero;

        if (HitTestUVPositionUncentered(ref uvWorldPosition))
        {
            Texture2D tex = CanvasInstanceMaterial.mainTexture as Texture2D;
            Texture2D outline = CanvasInstanceMaterial.GetTexture("_Outline") as Texture2D;

            //tex.FloodFillArea((int)(uvWorldPosition.x * tex.width), (int)(uvWorldPosition.y * tex.height), BrushColor, 10);
            tex.FloodFillArea(outline, (int)(uvWorldPosition.x * tex.width), (int)(uvWorldPosition.y * tex.height), BrushColor, 10);
            tex.Apply();
            CanvasInstanceMaterial.mainTexture = tex;
        }
    }

    //The main action, instantiates a brush or decal entity at the clicked position on the UV map
    void Paint()
    {
		Vector3 uvWorldPosition = Vector3.zero;

		if (HitTestUVPosition(ref uvWorldPosition))
        {
            CreateBrushes(uvWorldPosition);
		}

		if (_brushCounter >= MAX_BRUSH_COUNT) //If we reach the max brushes available, flatten the texture and clear the brushes
        {
            if (BrushCursor != null)
			    BrushCursor.SetActive(false);
		}
	}

    void CreateBrush(Vector3 position)
    {
        GameObject brushObj;
        brushObj = Instantiate(BrushPrefab); //Paint a brush
        brushObj.GetComponent<SpriteRenderer>().color = BrushColor; //Set the brush color
        //BrushColor.a = BrushSize * 2.0f; // Brushes have alpha to have a merging effect when painted over.
        brushObj.transform.parent = BrushContainer.transform; //Add the brush to our container to be wiped later
        brushObj.transform.localPosition = position; //The position of the brush (in the UVMap)
        brushObj.transform.localScale = Vector3.one * BrushSize;//The size of the brush

        _brushCounter++; //Add to the max brushes
    }

    void CreateBrushes(Vector3 position)
    {
        float dist = Vector3.Distance(_lastCursorPosition, position);

        for (float i = 0; i < dist; i += BrushSpacing)
        {
            Vector3 pos = Vector3.Lerp(_lastCursorPosition, position, i / dist);

            CreateBrush(pos);
        }

        _lastCursorPosition = position;
    }

	//Returns the position on the texuremap according to a hit in the mesh collider
	bool HitTestUVPosition(ref Vector3 uvWorldPosition)
    {
		RaycastHit hit;
		Vector3 cursorPos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0.0f);
		Ray cursorRay = SceneCamera.ScreenPointToRay (cursorPos);
		if (Physics.Raycast(cursorRay,out hit,200))
        {
			MeshCollider meshCollider = hit.collider as MeshCollider;
			if (meshCollider == null || meshCollider.sharedMesh == null)
				return false;			
			Vector2 pixelUV  = new Vector2(hit.textureCoord.x,hit.textureCoord.y);
			uvWorldPosition.x = pixelUV.x-CanvasCam.orthographicSize;//To center the UV on X
			uvWorldPosition.y = pixelUV.y-CanvasCam.orthographicSize;//To center the UV on Y
			uvWorldPosition.z = 0.0f;
			return true;
		}
		else
        {		
			return false;
		}
		
	}

    bool HitTestUVPositionUncentered(ref Vector3 uvWorldPosition)
    {
        RaycastHit hit;
        Vector3 cursorPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);
        Ray cursorRay = SceneCamera.ScreenPointToRay(cursorPos);
        if (Physics.Raycast(cursorRay, out hit, 200))
        {
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (meshCollider == null || meshCollider.sharedMesh == null)
                return false;
            Vector2 pixelUV = new Vector2(hit.textureCoord.x, hit.textureCoord.y);
            //uvWorldPosition.x = pixelUV.x - CanvasCam.orthographicSize;//To center the UV on X
            //uvWorldPosition.y = pixelUV.y - CanvasCam.orthographicSize;//To center the UV on Y
            //uvWorldPosition.z = 0.0f;
            uvWorldPosition = pixelUV;
            return true;
        }
        else
        {
            return false;
        }

    }

    //Sets the base material with a our canvas texture, then removes all our brushes
    void SaveTexture()
    {		
		_brushCounter = 0;
		System.DateTime date = System.DateTime.Now;
		RenderTexture.active = CanvasTexture;
		Texture2D tex = new Texture2D(CanvasTexture.width, CanvasTexture.height, TextureFormat.RGB24, false);		
		tex.ReadPixels(new Rect(0, 0, CanvasTexture.width, CanvasTexture.height), 0, 0);
		tex.Apply();
		RenderTexture.active = null;
		BaseMaterial.mainTexture = tex;	//Put the painted texture as the base

		foreach (Transform child in BrushContainer.transform)//Clear brushes
			Destroy(child.gameObject);

		//StartCoroutine ("SaveTextureToFile"); //Do you want to save the texture? This is your method!
	}

	////////////////// PUBLIC METHODS //////////////////

	public void SetBrushSize(float newBrushSize) //Sets the size of the cursor brush or decal
    {
		BrushSize = newBrushSize;

        if (BrushCursor != null)
            BrushCursor.transform.localScale = Vector3.one * BrushSize;
	}

    public void SetBrushColor(Color color)
    {
        BrushColor = color;
    }

	////////////////// OPTIONAL METHODS //////////////////

	#if !UNITY_WEBPLAYER 
	IEnumerator SaveTextureToFile(Texture2D savedTexture)
    {		
		_brushCounter = 0;
		string fullPath = System.IO.Directory.GetCurrentDirectory() + "\\UserCanvas\\";
		System.DateTime date = System.DateTime.Now;
		string fileName = "CanvasTexture.png";
		if (!System.IO.Directory.Exists(fullPath))		
			System.IO.Directory.CreateDirectory(fullPath);
		var bytes = savedTexture.EncodeToPNG();
		System.IO.File.WriteAllBytes(fullPath + fileName, bytes);
		Debug.Log("<color=orange>Saved Successfully!</color>" + fullPath + fileName);
		yield return null;
	}
	#endif
}
