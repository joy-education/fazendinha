﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutlineSelectionController : MonoBehaviour
{
    public string OutlinesPath = "PaintMode";
    public OutlineSelectionButton SelectionButtonPrefab;
    public RectTransform ButtonsArea;
    public TexturePainter PaintController;

    public Openable DrawingSelection;
    public Openable ColorPicker;
    public Openable BrushSize;
    public Openable DrawingOptions;
    public Openable PaintCanvas;

    public Sprite[] Outlines;

    private void Start()
    {
        LoadTextures();
        CreateOutlineButtons();
    }

    private void LoadTextures()
    {
        Outlines = Resources.LoadAll<Sprite>(OutlinesPath);
    }

    private void CreateOutlineButtons()
    {
        foreach (var outline in Outlines)
        {
            OutlineSelectionButton btn = Instantiate(SelectionButtonPrefab, ButtonsArea);
            btn.Sprite = outline;
            btn.Controller = this;
        }
    }

    public void SetCanvas(Texture2D outline)
    {
        AudioController.PlaySFX(AudioController.SFXList.DrawingPressed);

        DrawingSelection.SetActive(false);
        ColorPicker.SetActive(true);
        //BrushSize.SetActive(true);
        DrawingOptions.SetActive(true);

        PaintController.SetOutline(outline);
        PaintCanvas.SetActive(true);
    }
}
