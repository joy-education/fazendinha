﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
    public TexturePainter PainterManager;

    public void SetBrushColor(ColorPickerToggle toggle)
    {
        if (toggle.Toggle.isOn)
            PainterManager.BrushColor = toggle.Color;

        AudioController.PlaySFX(AudioController.SFXList.InkButtonPressed);
    }
}
