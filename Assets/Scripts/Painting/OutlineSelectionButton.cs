﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutlineSelectionButton : MonoBehaviour
{
    public OutlineSelectionController Controller { get; set; }

    public Sprite Sprite
    {
        get
        {
            return ((Image)Button.targetGraphic).sprite;
        }
        set
        {
            ((Image)Button.targetGraphic).sprite = value;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            Controller.SetCanvas(Sprite.texture);
        });
    }
}
