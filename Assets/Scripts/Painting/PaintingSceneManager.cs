﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PaintingSceneManager : MonoBehaviour
{
    public Openable Fader;

    private void Awake()
    {
        Fader.SetActive(false);
    }

    private void Start()
    {
        AudioController.PlayMusic(AudioController.MusicList.PaintMode);
    }

    public void BackToMainMenu()
    {
        AudioController.PlaySFX(AudioController.SFXList.MenuButtonPressed);
        AudioController.StopMusic();
        Fader.SetActive(true);
        AudioClip voice = AudioController.VoiceList.CurrentLocalization.Back;
        AudioController.PlayVoice(voice);
        CoroutineManager.WaitForSeconds(Mathf.Max(voice.length, Fader.Tween.Duration), () =>
        {
            SceneManager.LoadScene("MainMenu");
        });
    }
}
