﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class GameAudio
{
    public string Name;
    public AudioClip Clip;
}

[System.Serializable]
public class SFX
{
    public AudioClip Clip;
    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
            {
                _audioSource = new GameObject($"{Clip.name} Audio").AddComponent<AudioSource>();
                _audioSource.clip = Clip;
                _audioSource.spatialBlend = 0;
                _audioSource.playOnAwake = false;
                _audioSource.transform.SetParent(AudioManager.Instance.transform);
                _audioSource.outputAudioMixerGroup = AudioController.Instance.AudioMixer.FindMatchingGroups("SFX")[0];
            }

            return _audioSource;
        }
    }

    public SFX(AudioClip clip)
    {
        Clip = clip;
    }
}

[System.Serializable]
public class SFXList
{
    public AudioClip BrushSizePressed;
    public AudioClip ButtonEnterMenuAnimation;
    public AudioClip RightAnswer;
    public AudioClip DrawingPressed;
    public AudioClip InkButtonPressed;
    public AudioClip MenuButtonPressed;
    public AudioClip WrongAnswer;
}

[System.Serializable]
public class MusicList
{
    public AudioClip MainMenu;
    public AudioClip MathGames;
    public AudioClip PaintMode;
    public AudioClip WordsGames;
    public AudioClip LettersMode;
}

[System.Serializable]
public class VoiceList
{
    public LocalizedVoiceList PTBR;

    public LocalizedVoiceList CurrentLocalization
    {
        get
        {
            return PTBR;
        }
    }
}

[System.Serializable]
public class LocalizedVoiceList
{
    public AudioClip Addition;
    public AudioClip Back;
    public AudioClip CloseApp;
    public AudioClip Config;
    public AudioClip ConfigSound;
    public AudioClip Confirm;
    public AudioClip Congratz;
    public AudioClip Consonants;
    public AudioClip Continue;
    public AudioClip Correct;
    public AudioClip Credits;
    public AudioClip Dictation;
    public AudioClip LetsPlay;
    public AudioClip AlphabetLetters;
    public AudioClip Multiplication;
    public AudioClip Numbers;
    public AudioClip Painting;
    public AudioClip Play;
    public AudioClip PlayAgain;
    public AudioClip Subtraction;
    public AudioClip Title;
    public AudioClip ToMainMenu;
    public AudioClip TryAgain;
    public AudioClip Vowels;
    public AudioClip Welcome;
    public AudioClip Words;
    public AudioClip Wrong;
    public AudioClip Zero;
    public AudioClip One;
    public AudioClip Two;
    public AudioClip Three;
    public AudioClip Four;
    public AudioClip Five;
    public AudioClip Six;
    public AudioClip Seven;
    public AudioClip Eight;
    public AudioClip Nine;
    public AudioClip Ten;
    public AudioClip A;
    public AudioClip B;
    public AudioClip C;
    public AudioClip D;
    public AudioClip E;
    public AudioClip F;
    public AudioClip G;
    public AudioClip H;
    public AudioClip I;
    public AudioClip J;
    public AudioClip K;
    public AudioClip L;
    public AudioClip M;
    public AudioClip N;
    public AudioClip O;
    public AudioClip P;
    public AudioClip Q;
    public AudioClip R;
    public AudioClip S;
    public AudioClip T;
    public AudioClip U;
    public AudioClip V;
    public AudioClip W;
    public AudioClip X;
    public AudioClip Y;
    public AudioClip Z;
    public AudioClip AAgudo;
    public AudioClip ACircunflexo;
    public AudioClip ACrase;
    public AudioClip ATil;
    public AudioClip CCedilha;
    public AudioClip Cedilha;
    public AudioClip EAgudo;
    public AudioClip ECircunflexo;
    public AudioClip ECrase;
    public AudioClip IAgudo;
    public AudioClip ICrase;
    public AudioClip OAgudo;
    public AudioClip OCircunflexo;
    public AudioClip OCrase;
    public AudioClip OTil;
    public AudioClip UAgudo;
    public AudioClip UCircunflexo;
    public AudioClip UCrase;
}

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioManager>();
            if (_instance == null)
                _instance = new GameObject("AudioManager").AddComponent<AudioManager>();
            return _instance;
        }
    }
}

[CreateAssetMenu]
public class AudioController : ScriptableObject
{
    public AudioMixer AudioMixer;

    public SFXList SFX;
    public MusicList Music;
    public VoiceList Voice;

    public static SFXList SFXList
    {
        get
        {
            return AudioController.Instance.SFX;
        }
    }

    public static MusicList MusicList
    {
        get
        {
            return AudioController.Instance.Music;
        }
    }

    public static VoiceList VoiceList
    {
        get
        {
            return AudioController.Instance.Voice;
        }
    }

    private List<SFX> _sfx = new List<SFX>();

    private static AudioSource _musicAudioSource;
    public static AudioSource MusicAudioSource
    {
        get
        {
            if (_musicAudioSource == null)
            {
                _musicAudioSource = new GameObject("Music Audio").AddComponent<AudioSource>();
                _musicAudioSource.spatialBlend = 0;
                _musicAudioSource.playOnAwake = false;
                _musicAudioSource.loop = true;
                _musicAudioSource.transform.SetParent(AudioManager.Instance.transform);
                _musicAudioSource.outputAudioMixerGroup = Instance.AudioMixer.FindMatchingGroups("Music")[0];
            }

            return _musicAudioSource;
        }
    }

    private static AudioSource _voiceAudioSource;
    public static AudioSource VoiceAudioSource
    {
        get
        {
            if (_voiceAudioSource == null)
            {
                _voiceAudioSource = new GameObject("Voice Audio").AddComponent<AudioSource>();
                _voiceAudioSource.spatialBlend = 0;
                _voiceAudioSource.playOnAwake = false;
                _voiceAudioSource.loop = false;
                _voiceAudioSource.transform.SetParent(AudioManager.Instance.transform);
                _voiceAudioSource.outputAudioMixerGroup = Instance.AudioMixer.FindMatchingGroups("Voice")[0];
            }

            return _voiceAudioSource;
        }
    }

    private static AudioController _instance;
    public static AudioController Instance
    {
        get
        {
            return _instance;
        }
    }

    [RuntimeInitializeOnLoadMethod]
    private static void Init()
    {
        _instance = Resources.LoadAll<AudioController>("")[0];
    }

    public static void PlaySFX(AudioClip audio, System.Action callback = null)
    {

        SFX sfx = Instance._sfx.Find((s) => s.Clip == audio && !s.AudioSource.isPlaying);
        if (sfx == null)
        {
            sfx = new SFX(audio);
            Instance._sfx.Add(sfx);
        }
        sfx.AudioSource.Stop();
        sfx.AudioSource.Play();
    }

    public static void PlayMusic(AudioClip music)
    {
        MusicAudioSource.Stop();
        MusicAudioSource.clip = music;
        MusicAudioSource.Play();
    }

    public static void StopMusic()
    {
        MusicAudioSource.Stop();
    }

    public static void PlayVoice(AudioClip voice, System.Action callback = null)
    {
        VoiceAudioSource.Stop();
        VoiceAudioSource.clip = voice;
        VoiceAudioSource.Play();
        if (callback != null)
            CoroutineManager.WaitForSeconds(VoiceAudioSource.clip.length, callback);
    }

    public static void PlayVoice(AudioClip voice, float delay, System.Action callback = null)
    {
        CoroutineManager.WaitForSeconds(delay, () =>
        {
            PlayVoice(voice, callback);
        });
    }

    public static void PlayVoice(string name, System.Action callback = null)
    {
        AudioClip voice = null;
        switch (name.ToLower())
        {
            default:
                break;

            case "zero":
            case "0":
                voice = VoiceList.CurrentLocalization.Zero;
                break;

            case "one":
            case "1":
                voice = VoiceList.CurrentLocalization.One;
                break;

            case "two":
            case "2":
                voice = VoiceList.CurrentLocalization.Two;
                break;

            case "three":
            case "3":
                voice = VoiceList.CurrentLocalization.Three;
                break;

            case "four":
            case "4":
                voice = VoiceList.CurrentLocalization.Four;
                break;

            case "five":
            case "5":
                voice = VoiceList.CurrentLocalization.Five;
                break;

            case "six":
            case "6":
                voice = VoiceList.CurrentLocalization.Six;
                break;

            case "seven":
            case "7":
                voice = VoiceList.CurrentLocalization.Seven;
                break;

            case "eight":
            case "8":
                voice = VoiceList.CurrentLocalization.Eight;
                break;

            case "nine":
            case "9":
                voice = VoiceList.CurrentLocalization.Nine;
                break;

            case "ten":
            case "10":
                voice = VoiceList.CurrentLocalization.Ten;
                break;

            case "a":
                voice = VoiceList.CurrentLocalization.A;
                break;

            case "b":
                voice = VoiceList.CurrentLocalization.B;
                break;

            case "c":
                voice = VoiceList.CurrentLocalization.C;
                break;

            case "d":
                voice = VoiceList.CurrentLocalization.D;
                break;

            case "e":
                voice = VoiceList.CurrentLocalization.E;
                break;

            case "f":
                voice = VoiceList.CurrentLocalization.F;
                break;

            case "g":
                voice = VoiceList.CurrentLocalization.G;
                break;

            case "h":
                voice = VoiceList.CurrentLocalization.H;
                break;

            case "i":
                voice = VoiceList.CurrentLocalization.I;
                break;

            case "j":
                voice = VoiceList.CurrentLocalization.J;
                break;

            case "k":
                voice = VoiceList.CurrentLocalization.K;
                break;

            case "l":
                voice = VoiceList.CurrentLocalization.L;
                break;

            case "m":
                voice = VoiceList.CurrentLocalization.M;
                break;

            case "n":
                voice = VoiceList.CurrentLocalization.N;
                break;

            case "o":
                voice = VoiceList.CurrentLocalization.O;
                break;

            case "p":
                voice = VoiceList.CurrentLocalization.P;
                break;

            case "q":
                voice = VoiceList.CurrentLocalization.Q;
                break;

            case "r":
                voice = VoiceList.CurrentLocalization.R;
                break;

            case "s":
                voice = VoiceList.CurrentLocalization.S;
                break;

            case "t":
                voice = VoiceList.CurrentLocalization.T;
                break;

            case "u":
                voice = VoiceList.CurrentLocalization.U;
                break;

            case "v":
                voice = VoiceList.CurrentLocalization.V;
                break;

            case "w":
                voice = VoiceList.CurrentLocalization.W;
                break;

            case "x":
                voice = VoiceList.CurrentLocalization.X;
                break;

            case "y":
                voice = VoiceList.CurrentLocalization.Y;
                break;

            case "z":
                voice = VoiceList.CurrentLocalization.Z;
                break;

            case "á":
                voice = VoiceList.CurrentLocalization.AAgudo;
                break;

            case "â":
                voice = VoiceList.CurrentLocalization.ACircunflexo;
                break;

            case "ã":
                voice = VoiceList.CurrentLocalization.ATil;
                break;

            case "ç":
                voice = VoiceList.CurrentLocalization.Cedilha;
                break;

            case "é":
                voice = VoiceList.CurrentLocalization.EAgudo;
                break;

            case "ê":
                voice = VoiceList.CurrentLocalization.ECircunflexo;
                break;

            case "í":
                voice = VoiceList.CurrentLocalization.IAgudo;
                break;

            case "ó":
                voice = VoiceList.CurrentLocalization.OAgudo;
                break;

            case "ô":
                voice = VoiceList.CurrentLocalization.OCircunflexo;
                break;

            case "õ":
                voice = VoiceList.CurrentLocalization.OTil;
                break;

            case "ú":
                voice = VoiceList.CurrentLocalization.UAgudo;
                break;
        }

        PlayVoice(voice, callback);
    }

}
