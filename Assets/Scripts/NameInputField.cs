﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameInputField : MonoBehaviour {

    public Button Button;

    private InputField _inputField;
    public InputField InputField
    {
        get
        {
            if (_inputField == null)
                _inputField = GetComponent<InputField>();
            return _inputField;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Get(gameObject);
            return _tween;
        }
    }

    void Awake()
    {
        Button.onClick.AddListener(() =>
        {
            if (string.IsNullOrEmpty(InputField.text))
                return;

            GameConfigs.PlayerName = InputField.text;
            Button.interactable = false;
            Tween.Reverse(() => 
            {
                WordGameController.Instance.StartGame();
                gameObject.SetActive(false);
            });
        });
    }
}
