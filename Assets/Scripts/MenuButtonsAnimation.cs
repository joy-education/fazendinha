﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonsAnimation : MonoBehaviour
{
    public List<Button> Buttons;

    public void EnableButtons()
    {
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].gameObject.SetActive(true);

            PotaTween.Create(Buttons[i].gameObject).
                SetPosition(TweenAxis.Y, Buttons[i].GetComponent<RectTransform>().anchoredPosition.y - 900, Buttons[i].GetComponent<RectTransform>().anchoredPosition.y, true).
                SetEaseEquation(Ease.Equation.InOutBack).
                SetDelay(i * 0.1f).
                SetDuration(0.75f).
                Play();
        }
    }

    public void DisableButtons()
    {
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].gameObject.SetActive(false);
        }
    }
}
