﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundConfigPopup : MonoBehaviour
{
    public Slider MusicSlider;
    public Slider SFXSlider;
    public Slider VoiceSlider;

    private void Awake()
    {        
        MusicSlider.value = GetVolume("MusicVol");
        SFXSlider.value = GetVolume("SFXVol");
        VoiceSlider.value = GetVolume("VoiceVol");

        MusicSlider.onValueChanged.AddListener((value) => 
        {
            SetVolume("MusicVol", value);
        });

        SFXSlider.onValueChanged.AddListener((value) =>
        {
            SetVolume("SFXVol", value);
        });

        VoiceSlider.onValueChanged.AddListener((value) =>
        {
            SetVolume("VoiceVol", value);
        });
    }

    private void SetVolume(string key, float value)
    {
        AudioController.Instance.AudioMixer.SetFloat(key, Mathf.Log(value) * 20);
    }

    private float GetVolume(string key)
    {
        float val;
        AudioController.Instance.AudioMixer.GetFloat(key, out val);
        return Mathf.Exp(val / 20);
    }
}
