﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnimation : MonoBehaviour
{
    public TextProIndividualLetterPosition TextController;
    public Vector2 YFromTo = new Vector2(-1000, 0);
    public float TweensDuration = 0.5f;
    public float DelayBetweenLetters = 0.1f;

    private void OnEnable()
    {
        //SetActive(true, () => SetActive(false));
    }

    public void SetActive(bool active)
    {
        SetActive(active, null);
    }

    public void SetActive(bool active, System.Action callback)
    {
        gameObject.SetActive(true);
        if (active)
        {
            Play(callback);
        }
        else
        {
            Reverse(() => 
            {
                gameObject.SetActive(false);
                callback?.Invoke();
            });
        }
    }

    public void Play()
    {
        Play(null);
    }

    public void Play(System.Action callback)
    {
        for (int i = 0; i < TextController.Data.Count; i++)
        {
            if (i == TextController.Data.Count - 1)
                PlayTween(i, callback);
            else
                PlayTween(i);
        }
    }

    public void Reverse()
    {
        Reverse(null);
    }

    public void Reverse(System.Action callback)
    {
        for (int i = 0; i < TextController.Data.Count; i++)
        {
            if (i == TextController.Data.Count - 1)
                ReverseTween(i, callback);
            else
                ReverseTween(i);
        }
    }

    private void PlayTween(int tweenIndex, System.Action callback = null)
    {
        TextController.Data[tweenIndex].Translation.y = YFromTo.x;
        PotaTween.Create(gameObject, tweenIndex + 100)
            .SetFloat(YFromTo.x, YFromTo.y)
            .UpdateCallback(() => UpdateLetterPosition(tweenIndex))
            .SetDelay(tweenIndex * DelayBetweenLetters)
            .SetDuration(TweensDuration)
            .SetEaseEquation(Ease.Equation.OutBack)
            .Play(callback);
    }

    private void ReverseTween(int tweenIndex, System.Action callback = null)
    {
        TextController.Data[tweenIndex].Translation.y = YFromTo.y;
        PotaTween.Create(gameObject, tweenIndex + 100)
            .SetFloat(YFromTo.y, YFromTo.x)
            .UpdateCallback(() => UpdateLetterPosition(tweenIndex))
            .SetDelay(tweenIndex * DelayBetweenLetters)
            .SetDuration(TweensDuration)
            .SetEaseEquation(Ease.Equation.InBack)
            .Play(callback);
    }

    private void UpdateLetterPosition(int tweenIndex)
    {
        TextController.Data[tweenIndex].Translation.y = PotaTween.Get(gameObject, tweenIndex + 100).Float.Value;
    }
}
